﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Factories;
using FileApplication.Infrastructure.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unity;

namespace FileApplication.Infrastructure.Test
{
    [TestClass]
    public class FileSystemFunctionalTest : BaseTest
    {
        private bool CompareComponents(IFileSystemComponent f1, IFileSystemComponent f2)
        {
            var children1 = f1.ChildComponents.ToList();
            var children2 = f2.ChildComponents.ToList();

            var areEqual = f1.Name == f2.Name && children1.Count == children2.Count;

            if (areEqual)
            {
                foreach (var child1 in children1)
                {
                    var child2 = children2.FirstOrDefault(r => r.Name == child1.Name);
                    areEqual = child2 != null && CompareComponents(child1, child2);
                    if (areEqual) continue;

                    break;
                }
            }

            return areEqual;
        }

        private IFileSystemContainer BuildTestTree()
        {
            var factory = Container.Resolve<IFileSystemComponentFactory>();
            var root = factory.CreateRoot();
            var a1 = factory.CreateFolder("a1");
            var a2 = factory.CreateFolder("a2");
            var a3 = factory.CreateFolder("a3");
            var s1_1 = factory.CreateFile("s1_1");
            var s1_2 = factory.CreateFile("s1_2");
            var s2_1 = factory.CreateFile("s2_1.txt");

            root.Add(a1);
            a1.Add(s1_1);
            a1.Add(s1_2);
            a1.Add(a2);
            a2.Add(a3);
            a2.Add(s2_1);

            s2_1.Metadata.SetData(Encoding.ASCII.GetBytes("test"));

            return root;
        }

        private IFileSystemContainer BuildAndSaveTree()
        {
            var visitorFactory = Container.Resolve<IFileSystemVisitorFactory>();
            var factory = Container.Resolve<IFileSystemComponentFactory>();
            var folder = factory.CreateFolder("a1");
            var remove = visitorFactory.Delete();
            folder.Accept(remove);
            var root = BuildTestTree();
            var save = visitorFactory.Save();
            root.Accept(save);
            return root;
        }

        private IFileSystemContainer LoadTree()
        {
            var visitorFactory = Container.Resolve<IFileSystemVisitorFactory>();
            var componentFactory = Container.Resolve<IFileSystemComponentFactory>();
            var load = visitorFactory.LoadContainer();
            var folder = componentFactory.CreateRoot();
            folder.Accept(load);
            return folder;
        }

        [TestInitialize]
        public void InitTree()
        {

           
        }


        [TestMethod]
        public void when_savedStructureAndLoaded_expected_theSame()
        {
            var savedTree = BuildAndSaveTree();
            var loadedTree = LoadTree();
            Assert.IsTrue(CompareComponents(savedTree,loadedTree));
        }

        

        [TestMethod]
        public void when_loadStructureByPath_success()
        {
            var factory = Container.Resolve<IFileSystemComponentFactory>();
            var visitorFactory = Container.Resolve<IFileSystemVisitorFactory>();

            var savedTree = BuildAndSaveTree();
            var search = visitorFactory.GetChild<IFileSystemContainer>(r => r.Name == "a2");
            savedTree.Accept(search);
            var savedA2 = search.Component;
            Assert.IsNotNull(savedA2);

            var load = visitorFactory.LoadContainer(); 
            var a2 = factory.CreateFolder(@"a1\a2");
            a2.Accept(load);

            Assert.IsTrue(CompareComponents(savedA2, a2));

        }

       [TestMethod]
        public void when_deleteFolderByPath_success()
        {
            var factory = Container.Resolve<IFileSystemComponentFactory>();
            var visitorFactory = Container.Resolve<IFileSystemVisitorFactory>();
            var savedTree = BuildAndSaveTree();
            var folder = factory.CreateFolder("a1\\a2");
            var remove = visitorFactory.Delete();
            folder.Accept(remove);
            var loadedTree = LoadTree();
            var search = visitorFactory.GetChild<IFileSystemContainer>(r => r.Name == "a2");
            loadedTree.Accept(search);
            Assert.IsNull(search.Component);
        }

        [TestMethod]
        public void when_deleteFileByPath_success()
        {
            var factory = Container.Resolve<IFileSystemComponentFactory>();
            var visitorFactory = Container.Resolve<IFileSystemVisitorFactory>();
            var savedTree = BuildAndSaveTree();
            var componentToRemove = factory.CreateFile(@"a1\a2\s2_1.txt");
            var remove = visitorFactory.Delete();
            componentToRemove.Accept(remove);
            var loadedTree = LoadTree();
            var search = visitorFactory.GetChild<IFileSystemContainer>(r => r.Name == "s2_1.txt");
            loadedTree.Accept(search);
            Assert.IsNull(search.Component);
        }

        [TestMethod]
        public void when_renameComponentByPath_success()
        {
            var factory = Container.Resolve<IFileSystemComponentFactory>();
            var visitorFactory = Container.Resolve<IFileSystemVisitorFactory>();
            var savedTree = BuildAndSaveTree();
            var folderToRename = factory.CreateFolder(@"a1\a2\a3");
            var fileToRename = factory.CreateFile(@"a1\a2\s2_1.txt");
            
            var renameFile = visitorFactory.Rename("s2_1.log");
            fileToRename.Accept(renameFile);

            var renameFolder = visitorFactory.Rename("a3_");
            folderToRename.Accept(renameFolder);

            var loadedTree = LoadTree();

            var searchFile = visitorFactory.GetChild<IFileSystemComponent>(r => r.Name == "s2_1.txt");
            loadedTree.Accept(searchFile);
            Assert.IsNull(searchFile.Component);

            searchFile = visitorFactory.GetChild<IFileSystemComponent>(r => r.Name == "s2_1.log");
            loadedTree.Accept(searchFile);
            Assert.IsNotNull(searchFile.Component);

            var searchFolder = visitorFactory.GetChild<IFileSystemContainer>(r => r.Name == "a3");
            loadedTree.Accept(searchFolder);
            Assert.IsNull(searchFolder.Component);

            searchFolder = visitorFactory.GetChild<IFileSystemContainer>(r => r.Name == "a3_");
            loadedTree.Accept(searchFolder);
            Assert.IsNotNull(searchFolder.Component);


        }

        [TestMethod]
        public void when_createCopyOfFolderByPath_success()
        {
            var factory = Container.Resolve<IFileSystemComponentFactory>();
            var visitorFactory = Container.Resolve<IFileSystemVisitorFactory>();
            var savedTree = BuildAndSaveTree();
            var load = visitorFactory.LoadContainer();
            var makeCopy = visitorFactory.Copy();
            var folder = factory.CreateFolder("a1\\a2");
            folder.Accept(load);
            folder.Accept(makeCopy);

            var loadedTree = LoadTree();
            var search = visitorFactory.GetChild<IFileSystemContainer>(r => r.Name == "Copy Of a2");
            loadedTree.Accept(search);
            Assert.IsNotNull(search.Component);
        }

        [TestMethod]
        public void when_createCopyOfFileByPath_success()
        {
            var factory = Container.Resolve<IFileSystemComponentFactory>();
            var visitorFactory = Container.Resolve<IFileSystemVisitorFactory>();
            var savedTree = BuildAndSaveTree();
            var makeCopy = visitorFactory.Copy();
            var file = factory.CreateFile(@"a1\a2\s2_1.txt");
            file.Accept(makeCopy);

            var loadedTree = LoadTree();
            var search = visitorFactory.GetChild<IFileSystemComponent>(r => r.Name == "Copy Of s2_1.txt");
            loadedTree.Accept(search);
            Assert.IsNotNull(search.Component);
        }

        [TestMethod]
        public void when_readFileByPath_success()
        {
            var factory = Container.Resolve<IFileSystemComponentFactory>();
            var visitorFactory = Container.Resolve<IFileSystemVisitorFactory>();
            var savedTree = BuildAndSaveTree();
            var readData = visitorFactory.Read();
            var file = factory.CreateFile(@"a1\a2\s2_1.txt");
            file.Accept(readData);
            var text = Encoding.ASCII.GetString(readData.Data);
            Assert.IsNotNull("test",text);
        }


        [TestMethod]
        public void when_createSubFolder_success()
        {
            var factory = Container.Resolve<IFileSystemComponentFactory>();
            var visitorFactory = Container.Resolve<IFileSystemVisitorFactory>();
            var savedTree = BuildAndSaveTree();

            var search = visitorFactory.GetChild<IFileSystemContainer>(r => r.Name == "a1");
            savedTree.Accept(search);

            var subFolder = factory.CreateFolder("sub folder");
            search.Component.Add(subFolder);

            var save = visitorFactory.Save();
            search.Component.Accept(save);

            var loadedTree = LoadTree();
            var searchSubFolder = visitorFactory.GetChild<IFileSystemComponent>(r => r.Name == "sub folder");
            loadedTree.Accept(searchSubFolder);
            Assert.IsNotNull(searchSubFolder.Component);
        }

        [TestMethod]
        public void when_createFileInFolder_success()
        {
            var factory = Container.Resolve<IFileSystemComponentFactory>();
            var visitorFactory = Container.Resolve<IFileSystemVisitorFactory>();
            var savedTree = BuildAndSaveTree();

            var load = visitorFactory.LoadContainer();
            var folder = factory.CreateFolder("a1\\a2");
            folder.Accept(load);

            var file = factory.CreateFile("newFile.txt");
            folder.Add(file);

            var save = visitorFactory.Save();
            folder.Accept(save);

            var loadedTree = LoadTree();
            var searchNewFile = visitorFactory.GetChild<IFileSystemComponent>(r => r.Name == "newFile.txt");
            loadedTree.Accept(searchNewFile);
            Assert.IsNotNull(searchNewFile.Component);
        }

        [TestMethod]
        public void when_calculateFolderSize_success()
        {
            var visitorFactory = Container.Resolve<IFileSystemVisitorFactory>();
            var savedTree = BuildAndSaveTree();

            var calculateSize = visitorFactory.Size();
            savedTree.Accept(calculateSize);
            
            Assert.AreEqual(4, calculateSize.Size);
        }
    }
}
