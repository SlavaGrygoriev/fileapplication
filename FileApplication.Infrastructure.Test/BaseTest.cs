﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileApplication.Infrastructure.Factories;
using FileApplication.Infrastructure.Interfaces;
using FileApplication.Infrastructure.Providers;
using FileApplication.Infrastructure.Repositories;
using Unity;

namespace FileApplication.Infrastructure.Test
{
    public abstract class BaseTest
    {
        protected UnityContainer Container { get; }

        protected BaseTest()
        {
            Container = new UnityContainer();
            Container.RegisterInstance<ILocalStoragePathProvider>(
                new LocalStoragePathProvider("d:\\temp\\FileApplicationStorage"));
            Container.RegisterType<IFileSystemComponentFactory, FileSystemComponentFactory>();
            Container.RegisterType<IFileSystemComponentRepository, FileSystemComponentRepository>();
            Container.RegisterType<IFileSystemVisitorFactory, FileSystemVisitorFactory>();
        }
    }
}
