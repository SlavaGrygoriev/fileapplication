﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Unity;

namespace FileApplication
{
    public static class DependencyResolver
    {
        public static IUnityContainer Current { get; private set; }

        public static void SetCurrentContainer(IUnityContainer container)
        {
            Current = container;
        }
    }
}