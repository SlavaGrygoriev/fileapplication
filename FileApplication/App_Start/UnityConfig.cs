using System;
using System.Web.Http;
using FileApplication.Infrastructure.Factories;
using FileApplication.Infrastructure.Interfaces;
using FileApplication.Infrastructure.Providers;
using FileApplication.Infrastructure.Repositories;
using Unity;
using Unity.WebApi;

namespace FileApplication
{
    public static class UnityConfig
    {
        private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();

            RegisterTypes(container);
            DependencyResolver.SetCurrentContainer(container);
            return container;
        });

        private static void RegisterTypes(UnityContainer container)
        {
            container.RegisterInstance<ILocalStoragePathProvider>(
                new LocalStoragePathProvider("d:\\temp\\FileApplicationStorage"));
            container.RegisterType<IFileSystemComponentFactory, FileSystemComponentFactory>();
            container.RegisterType<IFileSystemComponentRepository, FileSystemComponentRepository>();
            container.RegisterType<IFileSystemVisitorFactory, FileSystemVisitorFactory>();
        }

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return Container.Value;
        }

    }
}