﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Interfaces;
using FileApplication.Models;

namespace FileApplication.Infrastructure
{
    public class ComponentMapper : IFileSystemVisitor
    {
        private readonly Stack<FileSystemComponentDto> _branches = new Stack<FileSystemComponentDto>();
        private FileSystemComponentDto Branch => _branches.Peek();

        public FileSystemComponentDto Component { get; private set; }


        private FileSystemComponentDto CreateFrom(IFileSystemContainer component)
        {
            var dto = new FileSystemComponentDto
            {
                IsContainer = true,
                Name = component.Name,
                FullName = component.Metadata.GetPath()
            };

            return dto;
        }

        private FileSystemComponentDto CreateFrom(IFileSystemComponent component)
        {
            var dto = new FileSystemComponentDto
            {
                IsContainer = false,
                Name = component.Name,
                FullName = component.Metadata.GetPath()
            };
            return dto;
        }

        public bool Visit(Folder folder)
        {
            var dto = CreateFrom(folder);
            if (Component == null)
            {
                if (string.IsNullOrEmpty(dto.Name))
                {
                    dto.Name = "\\";
                }
                Component = dto;

            }
            else
            {
                Branch.Children.Add(dto);
            }
            _branches.Push(dto);
            return true;
        }

        public bool Visit(File file)
        {
            var singleFileCopy = Component == null;
            var dto = CreateFrom(file);
            if (singleFileCopy)
            {
                Component = dto;
            }
            else
            {
                Branch.Children.Add(dto);
            }
            return !singleFileCopy;
        }

        public bool VisitContainer(IFileSystemContainer component)
        {
            return true;
        }

        public void CompleteContainer(IFileSystemContainer component)
        {
            _branches.Pop();
        }
    }
}