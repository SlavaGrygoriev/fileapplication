﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileApplication.Models
{
    public class FileSystemComponentDto
    {
        public bool IsContainer { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public List<FileSystemComponentDto> Children { get; set; } = new List<FileSystemComponentDto>();

    }
}