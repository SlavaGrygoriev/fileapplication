﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FileApplication.Infrastructure;
using FileApplication.Infrastructure.Interfaces;
using FileApplication.Models;

namespace FileApplication.Controllers
{
    public class FolderController : ApiController
    {
        private readonly IFileSystemComponentFactory _componentFactory;
        private readonly IFileSystemVisitorFactory _visitorFactory;

        public FolderController(IFileSystemComponentFactory componentFactory,IFileSystemVisitorFactory visitorFactory)
        {
            _componentFactory = componentFactory;
            _visitorFactory = visitorFactory;
        }

        public FileSystemComponentDto Get()
        {
            var load = _visitorFactory.LoadContainer();
            var folder = _componentFactory.CreateRoot();
            folder.Accept(load);

            var mapToDto = new ComponentMapper();
            folder.Accept(mapToDto);

            return mapToDto.Component;
        }

        public FileSystemComponentDto Get(string path)
        {
            var load = _visitorFactory.LoadContainer();
            var folder = _componentFactory.CreateFolder(path);
            folder.Accept(load);
            var mapToDto = new ComponentMapper();
            folder.Accept(mapToDto);

            return mapToDto.Component;
        }

        public void Post(string path,[FromBody]FileSystemComponentDto dto)
        {
            var folder = _componentFactory.CreateFolder(path);
            var subFolder = _componentFactory.CreateFolder(dto.Name);
            folder.Add(subFolder);
            var save = _visitorFactory.Save();
            folder.Accept(save);
        }

        //rename
        public void Put(string path, [FromBody]FileSystemComponentDto dto)
        {
            var rename = _visitorFactory.Rename(dto.Name);
            var folder = _componentFactory.CreateFolder(path);
            folder.Accept(rename);
        }

        public void Delete(string path)
        {
            var remove = _visitorFactory.Delete();
            var folder = _componentFactory.CreateFolder(path);
            folder.Accept(remove);
        }


        [Route("api/folder/copy")]
        [HttpPost]
        public FileSystemComponentDto Copy(string path)
        {
            var folder = _componentFactory.CreateFolder(path);
            var makeCopy = _visitorFactory.Copy();
            folder.Accept(makeCopy);
            var copy = makeCopy.Component;
            var mapToDto = new ComponentMapper();
            copy.Accept(mapToDto);
            return mapToDto.Component;
        }
        [Route("api/folder/metadata")]
        [HttpGet]
        public IEnumerable<FileSystemComponentMetadataDto> Metadata(string path)
        {
            var calcSize = _visitorFactory.Size();
            var folder = _componentFactory.CreateFolder(path);
            folder.Accept(calcSize);
            return folder.Metadata.Select(r => new FileSystemComponentMetadataDto()
            {
                Key = r.Key,
                Value = r.Value
            });
        }
    }
}
