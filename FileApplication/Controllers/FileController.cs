﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using FileApplication.Infrastructure;
using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Interfaces;
using FileApplication.Models;

namespace FileApplication.Controllers
{
    public class FileController : ApiController
    {
        private readonly IFileSystemComponentFactory _componentFactory;
        private readonly IFileSystemVisitorFactory _visitorFactory;

        public FileController(IFileSystemComponentFactory componentFactory,IFileSystemVisitorFactory visitorFactory)
        {
            _componentFactory = componentFactory;
            _visitorFactory = visitorFactory;
        }


        public void Post(string path,[FromBody]FileSystemComponentDto dto)
        {
            var file = _componentFactory.CreateFolder(path);
            var subFolder = _componentFactory.CreateFolder(dto.Name);
            file.Add(subFolder);
            var save = _visitorFactory.Save();
            file.Accept(save);
        }

        //rename
        public void Put(string path, [FromBody]FileSystemComponentDto dto)
        {
            var rename = _visitorFactory.Rename(dto.Name);
            var file = _componentFactory.CreateFile(path);
            file.Accept(rename);
        }

        public void Delete(string path)
        {
            var remove = _visitorFactory.Delete();
            var file = _componentFactory.CreateFile(path);
            file.Accept(remove);
        }


        [Route("api/file/copy")]
        [HttpPost]
        public FileSystemComponentDto Copy(string path)
        {
            var file = _componentFactory.CreateFile(path);
            var makeCopy = _visitorFactory.Copy();
            file.Accept(makeCopy);
            var copy = makeCopy.Component;
            var mapToDto = new ComponentMapper();
            copy.Accept(mapToDto);
            return mapToDto.Component;
        }

        [Route("api/file/download")]
        [HttpGet]
        public HttpResponseMessage Download(string path)
        {
            var file = _componentFactory.CreateFile(path);
            var read = _visitorFactory.Read();
            file.Accept(read);
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(new MemoryStream(read.Data))
            };

            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment") { FileName = file.Name };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain"); //todo: dynamic mime type

            return response;

        }

        [Route("api/file/upload")]
        [HttpPost]
        public async Task<IHttpActionResult> Upload(string path)
        {
            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);
            var uploadedFile = provider.Contents.First();
            var fileName = uploadedFile.Headers.ContentDisposition.FileName.Trim('\"');
            var fullName = Path.Combine(path, fileName);
            var data = await uploadedFile.ReadAsByteArrayAsync();
            var file = _componentFactory.CreateFile(fullName);
            file.Metadata.SetData(data);
            return Ok();
        }

        [Route("api/file/metadata")]
        [HttpGet]
        public IEnumerable<FileSystemComponentMetadataDto> Metadata(string path)
        {
            var calcSize = _visitorFactory.Size();
            var file = _componentFactory.CreateFile(path);
            file.Accept(calcSize);
            return file.Metadata.Select(r=>new FileSystemComponentMetadataDto()
            {
                Key = r.Key,
                Value = r.Value
            });
        }
    }
}
