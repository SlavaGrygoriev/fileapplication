# File Application 
> Note  
> Unfortunately, I did not have enough time to create the UI for this task. I spent about 15 hours working 2-3 hours every evening.  As I will not be able to work on this weekend so I decided to show what was done.

## Introduction  
File System is a tree structure of folders and files. A folder is a container which may have files. An application Domain reflects this structure and its components relations. The application consists of 2 parts: 
  1. Web site
  2. Class library which contains domain model and functionality to work with.
The application uses DI container to initialize repository and factories with their dependencies.

## Domain model
FileSystemComponent class presents any component of the structure. It can be a leaf(File) or a container(Folder). So, File system is a composition of components. Each component has Name and a dictionary of metadata. For instance, a component size is metadata item with specific key. By this way the component can have many custom attributes.

### Components factory
The factory intends to provide a logic to create and initialize any components of the structure. 

## Folder and File actions
Pattern Visitor is used to provide actions for any node of the components structure. This approach allows to maintain any behavior on the tree. 

## Components Storage
All components are stored in a repository. The storage can be real file system or database. Interface IFileSystemComponentRepository describes how client code can interact with the repository using simple operations. The local file storage uses Path provider to local hard drive where the folders and files are saved. So, the provider must be setup for each local PC.

### Using a database to store the structure
There should be a class implemented IFileSystemComponentRepository interface. It could keep the structure in tables:

  - Component: 

    - id
    - parent id
    - path
  
  - ComponentData: to keep blob of component

    - componentID
    - varbinary

  - ComponenetMetadata: to store metadada

    - componentID
    - key
    - value

The class must registered in the DI container.

## Unit test
There are functional unit tests to see how it can be used .

## Supporting new components

To add a new type of component a developer should:

 - Create new class inherited from FileSystemComponent
 - Change visitor interface to handle new component 

  