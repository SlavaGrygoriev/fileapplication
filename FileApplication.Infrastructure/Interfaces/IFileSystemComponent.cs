﻿using System.Collections.Generic;
using FileApplication.Infrastructure.Domain;

namespace FileApplication.Infrastructure.Interfaces
{
    public interface IFileSystemComponent
    {
        IEnumerable<IFileSystemComponent> ChildComponents { get; }
        string Name { get; }

        bool Accept(IFileSystemVisitor visitor);

        IList<MetadataItem> Metadata { get; }
    }
}