﻿using System.Collections.Generic;
using FileApplication.Infrastructure.Domain;

namespace FileApplication.Infrastructure.Interfaces
{
    public interface IFileSystemComponentRepository
    {
        void Create(FileInfo fileInfo, byte[] data);
        void Create(FolderInfo container);
        void Delete(FileInfo file);
        void Delete(FolderInfo folder);
        bool Exists(FileInfo file);
        bool Exists(FolderInfo container);
        IEnumerable<FileInfo> GetFiles(FolderInfo container);
        IEnumerable<FolderInfo> GetFolders(FolderInfo container);
        long GetSize(FileInfo fileInfo);
        byte[] Read(FileInfo fileInfo);
        void Rename(FileInfo file,string newName);
        void Rename(FolderInfo folder, string newName);
    }
}