﻿using FileApplication.Infrastructure.Domain;

namespace FileApplication.Infrastructure.Interfaces
{
    public interface IFileSystemVisitor
    {
        bool Visit(Folder folder);
        bool Visit(File file);
        bool VisitContainer(IFileSystemContainer component);
        void CompleteContainer(IFileSystemContainer component);
    }

}
