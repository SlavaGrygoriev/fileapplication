﻿namespace FileApplication.Infrastructure.Interfaces
{
    public interface IFileSystemContainer  : IFileSystemComponent
    {
        void Add(IFileSystemComponent component);
        void Remove(IFileSystemComponent component);

    }
}