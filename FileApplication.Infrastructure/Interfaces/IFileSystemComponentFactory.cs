﻿using FileApplication.Infrastructure.Domain;

namespace FileApplication.Infrastructure.Interfaces
{
    public interface IFileSystemComponentFactory
    {
        IFileSystemComponent CreateFile(string name);
        IFileSystemContainer CreateFolder(string name);
        IFileSystemContainer CreateRoot();
    }
}