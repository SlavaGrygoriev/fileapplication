﻿using System;
using FileApplication.Infrastructure.Visitors;

namespace FileApplication.Infrastructure.Interfaces
{
    public interface IFileSystemVisitorFactory
    {
        IFileSystemVisitor LoadContainer();
        IFileSystemVisitor Save();
        GetChildComponentVisitor<T> GetChild<T>(Func<T, bool> expr) where T : class, IFileSystemComponent;

        IFileSystemVisitor Delete();
        IFileSystemVisitor Rename(string newName);
        CopyComponentVisitor Copy(Func<IFileSystemComponent, string> copyNameExpr = null);
        IFileSystemVisitor Write(byte[] data);
        ReadDataVisitor Read();
        SizeComponentVisitor Size();
    }
}