﻿namespace FileApplication.Infrastructure.Interfaces
{
    public interface ILocalStoragePathProvider
    {
        string Path { get; }
    }
}