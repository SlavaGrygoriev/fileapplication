﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileApplication.Infrastructure.Interfaces;
using FileApplication.Infrastructure.Visitors;

namespace FileApplication.Infrastructure.Domain
{
    public abstract class FileSystemContainer : FileSystemComponent, IFileSystemContainer
    {

        public void Remove(IFileSystemComponent component)
        {
            var itemToRemove = Children.FirstOrDefault(r => r == component);

            if (itemToRemove == null)
            {
                throw new KeyNotFoundException(component.ToString());
            }

            var child = itemToRemove.ChildComponents.FirstOrDefault();
            while (child != null)
            {
                child = itemToRemove.ChildComponents.FirstOrDefault();
            }

            Children.Remove(itemToRemove);
        }


        public void Add(IFileSystemComponent component)
        {
            Children.Add(component);
            component.Metadata.SetPath(this.Metadata.GetPath() + "\\" + component.Name);
        }


        public override bool Accept(IFileSystemVisitor visitor)
        {
            visitor.VisitContainer(this);

            var result = AcceptVisitor(visitor);
            if (result && Children.Count > 0)
            {
                foreach (var component in ChildComponents.ToList())
                {
                    var progress = component.Accept(visitor);
                    if (!progress)
                    {
                        break;
                    }
                }
            }

            visitor.CompleteContainer(this);

            return result;
        }

    }
}
