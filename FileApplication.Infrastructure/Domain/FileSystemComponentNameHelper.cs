﻿using System.Linq;

namespace FileApplication.Infrastructure.Domain
{
    public static class FileSystemComponentNameHelper
    {
        private static char Separator = '\\';

        public static bool IsPath(this string fullName)
        {
            return fullName?.Contains(Separator) == true;
        }

        public static string ExtractName(this string fullName)
        {
            return fullName?.Split(Separator).Last();
        }

        public static string GetParentPath(this string fullName)
        {
            var parts = fullName.Split(Separator);
            if (parts.Length == 1) return "";
            return string.Join(Separator.ToString(), parts, 0, parts.Length - 1);
        }

        public static string ReplaceName(this string fullName,string newName)
        {
            var parts = fullName.Split(Separator);
            parts[parts.Length - 1] = newName;
            return string.Join(Separator.ToString(), parts);
        }
    }
}
