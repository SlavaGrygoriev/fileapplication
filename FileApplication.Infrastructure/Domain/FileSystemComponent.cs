﻿using System.Collections.Generic;
using FileApplication.Infrastructure.Interfaces;

namespace FileApplication.Infrastructure.Domain
{
    public abstract class FileSystemComponent : IFileSystemComponent
    {
        public IEnumerable<IFileSystemComponent> ChildComponents => Children;

        protected List<IFileSystemComponent> Children { get; set; } = new List<IFileSystemComponent>();

        public string Name { get; set; }

        public abstract bool Accept(IFileSystemVisitor visitor);
        public IList<MetadataItem> Metadata { get; } = new List<MetadataItem>();

        protected abstract bool AcceptVisitor(IFileSystemVisitor visitor);

    }
}
