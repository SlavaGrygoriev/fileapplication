﻿using System.Diagnostics;
using FileApplication.Infrastructure.Interfaces;

namespace FileApplication.Infrastructure.Domain
{
    [DebuggerDisplay("File:{Name}")]
    public class File : FileSystemComponent
    {
        public File(){}

        public File(string name)
        {
            Name = name;
        }

        public byte[] Data
        {
            get => this.Metadata.GetData();
            set => this.Metadata.SetData(value);
        }

        public long Size => this.Metadata.GetSize();

        public override bool Accept(IFileSystemVisitor visitor)
        {
            return AcceptVisitor(visitor);
        }

        protected override bool AcceptVisitor(IFileSystemVisitor visitor)
        {
            return visitor.Visit(this);
        }
    }
}
