﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileApplication.Infrastructure.Domain
{
    public static class MetadataHelper
    {
        public static class MetaKeys
        {
            public const string Path = "path";
            public const string Data = "data";
            public const string Size = "size";
        }

        public static long GetSize(this IEnumerable<MetadataItem> items)
        {
            long result = 0;
            var item = items.GetItem(MetaKeys.Size);
            if (item != null)
            {
                long.TryParse(item.Value, out result);
            }
            return result;
        }

        public static MetadataItem SetSize(this IList<MetadataItem> items, long value)
        {
            return items.SetItemValue(MetaKeys.Size, value.ToString());
        }

        public static MetadataItem GetItem(this IEnumerable<MetadataItem> items,string key)
        {
            return items.FirstOrDefault(r => string.Equals(key, r.Key, StringComparison.InvariantCultureIgnoreCase));
        }

        public static byte[] GetData(this IEnumerable<MetadataItem> items)
        {
            var item = items.GetItem(MetaKeys.Data);
            return item == null ? null : Encoding.ASCII.GetBytes(item.Value); 
        }

        public static MetadataItem SetData(this IList<MetadataItem> items, byte[] data)
        {
            var value = Encoding.ASCII.GetString(data);
            return items.SetItemValue( MetaKeys.Data,value);
        }

        public static string GetPath(this IEnumerable<MetadataItem> items)
        {
            var item = items.GetItem(MetaKeys.Path);
            return item == null ? "" : item.Value;
        }

        public static MetadataItem SetItemValue(this IList<MetadataItem> items,string key, string value)
        {
            var item = items.GetItem(key);
            if (item == null)
            {
                item = new MetadataItem()
                {
                    Key = key,
                };
                items.Add(item);
            }

            item.Value = value;
            return item;
        }


        public static MetadataItem SetPath(this IList<MetadataItem> items,string path)
        {
            return items.SetItemValue(MetaKeys.Path, path);
        }
    }
}
