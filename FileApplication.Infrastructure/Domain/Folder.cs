﻿using System.Diagnostics;
using FileApplication.Infrastructure.Interfaces;

namespace FileApplication.Infrastructure.Domain
{
    [DebuggerDisplay("Folder:{Name}")]
    public class Folder : FileSystemContainer
    {
        public Folder()
        {

        }

        public bool IsRoot { get; set; }

        public Folder(string name)
        {
            Name = name;
        }
        
        protected override bool AcceptVisitor(IFileSystemVisitor visitor)
        {
            return visitor.Visit(this);
        }

      
    }
}
