﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileApplication.Infrastructure.Domain
{
    public class FolderInfo
    {
        public FolderInfo()
        {

        }

        public FolderInfo(string name)
        {
            Name = name;
        }
        public string Name { get; set; }
    }
}
