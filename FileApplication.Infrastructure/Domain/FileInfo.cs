﻿namespace FileApplication.Infrastructure.Domain
{
    public class FileInfo
    {
        public FileInfo()
        {

        }

        public FileInfo(string name)
        {
            Name = name;
        }
        public string Name { get; set; }
        //public byte[] Data { get; set; }
    }
}
