﻿namespace FileApplication.Infrastructure.Domain
{
    public class MetadataItem
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
