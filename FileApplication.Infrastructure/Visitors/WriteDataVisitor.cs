﻿using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Interfaces;

namespace FileApplication.Infrastructure.Visitors
{
    public class WriteDataVisitor : BaseComponentVisitor
    {
        private readonly IFileSystemComponentRepository _componentRepository;
        private readonly byte[] _data;

        public WriteDataVisitor(IFileSystemComponentRepository componentRepository, byte[] data)
        {
            _componentRepository = componentRepository;
            _data = data;
        }
        public override bool Visit(Folder folder)
        {
            return false;
        }

        public override bool Visit(File file)
        {
            var path = file.Metadata.GetPath();
            _componentRepository.Create(new FileInfo(path),_data);
            return false;
        }

    }
}
