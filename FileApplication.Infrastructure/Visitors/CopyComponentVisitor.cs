﻿using System;
using System.Collections.Generic;
using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Factories;
using FileApplication.Infrastructure.Interfaces;

namespace FileApplication.Infrastructure.Visitors
{
    public class CopyComponentVisitor : BaseComponentVisitor
    {
        public IFileSystemComponent Component { get; set; }
        private readonly Stack<IFileSystemContainer> _branches = new Stack<IFileSystemContainer>();
        private IFileSystemContainer Branch => _branches.Peek();
        private readonly IFileSystemComponentFactory _componentFactory;
        private readonly IFileSystemComponentRepository _componentRepository;
        private readonly Func<IFileSystemComponent, string> _copyNameExpr;

        public CopyComponentVisitor(IFileSystemComponentFactory componentFactory,
            IFileSystemComponentRepository componentRepository,
            Func<IFileSystemComponent,string> copyNameExpr)
        {
            _componentFactory = componentFactory;
            _componentRepository = componentRepository;
            _copyNameExpr = copyNameExpr;
        }

        public override bool Visit(Folder folder)
        {
            var name = folder.Name;
            if (Component == null)
            {
                name = folder.Metadata.GetPath().ReplaceName(_copyNameExpr(folder)); 
            }

            var copy = _componentFactory.CreateFolder(name);
            if (Component == null)
            {
                Component = copy;
            }
            else
            {
                Branch.Add(copy);
            }
            var path = copy.Metadata.GetPath();
            _componentRepository.Create(new FolderInfo(path));
            _branches.Push(copy);

            return true;
        }

        public override bool Visit(File file)
        {
            var singleFileCopy = Component == null;
            var sourcePath = file.Metadata.GetPath();
            var data = _componentRepository.Read(new FileInfo(sourcePath));
            var name = file.Name;
            if (singleFileCopy)
            {
                name = file.Metadata.GetPath().ReplaceName(_copyNameExpr(file));
            }

            var copy = _componentFactory.CreateFile(name);
            if (singleFileCopy)
            {
                Component = copy;
            }
            else
            {
                Branch.Add(copy);
            }
            var copyPath = copy.Metadata.GetPath();
            _componentRepository.Create(new FileInfo(copyPath),data);
            return !singleFileCopy;
        }

        public override void CompleteContainer(IFileSystemContainer component)
        {
            _branches.Pop();
        }
    }
}
