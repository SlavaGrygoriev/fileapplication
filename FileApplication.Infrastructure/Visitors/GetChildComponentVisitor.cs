﻿using System;
using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Interfaces;

namespace FileApplication.Infrastructure.Visitors
{
    public class GetChildComponentVisitor<T> : BaseComponentVisitor where T: class, IFileSystemComponent
    {
        private readonly Func<T, bool> _expr;

        public T Component { get; private set; }

        public GetChildComponentVisitor(Func<T, bool> expr)
        {
            _expr = expr;
        }

        private bool Find(T component)
        {
            var found = _expr(component);
            if (found)
            {
                Component = component;
            }
            return found;
        }

        public override bool Visit(Folder folder)
        {
            var found = (IFileSystemComponent)folder is T component && Find(component);
            return !found;
        }

        public override bool Visit(File file)
        {
            var found = (IFileSystemComponent)file is T component && Find(component);
            return !found;
        }

    }
}
