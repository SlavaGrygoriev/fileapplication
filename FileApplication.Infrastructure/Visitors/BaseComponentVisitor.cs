﻿using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Interfaces;

namespace FileApplication.Infrastructure.Visitors
{
    public abstract class BaseComponentVisitor : IFileSystemVisitor
    {

        public abstract bool Visit(Folder folder);

        public abstract bool Visit(File file);

        public virtual bool VisitContainer(IFileSystemContainer component)
        {
            return true;
        }

        public virtual void CompleteContainer(IFileSystemContainer component)
        {
            
        }
    }
}
