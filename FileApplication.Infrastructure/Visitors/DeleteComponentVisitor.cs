﻿using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Interfaces;

namespace FileApplication.Infrastructure.Visitors
{
    public class DeleteComponentVisitor : BaseComponentVisitor
    {
        private readonly IFileSystemComponentRepository _componentRepository;

        public DeleteComponentVisitor(IFileSystemComponentRepository componentRepository)
        {
            _componentRepository = componentRepository;
        }

        public override bool Visit(Folder folder)
        {
            return !folder.IsRoot;
        }

        public override bool Visit(File file)
        {
            var path = file.Metadata.GetPath();
            var info = new FileInfo()
            {
                Name = path,
            };
            if (_componentRepository.Exists(info))
            {
                _componentRepository.Delete(info);
            }
            return true;
        }

        public override void CompleteContainer(IFileSystemContainer component)
        {
            var path = component.Metadata.GetPath();
            var info = new FolderInfo()
            {
                Name = path,
            };
            if (_componentRepository.Exists(info))
            {
                _componentRepository.Delete(info);
            }
        }
    }
}
