﻿using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Interfaces;

namespace FileApplication.Infrastructure.Visitors
{
    public class ReadDataVisitor : BaseComponentVisitor
    {
        private readonly IFileSystemComponentRepository _componentRepository;
        public byte[] Data { get; private set; }

        public ReadDataVisitor(IFileSystemComponentRepository componentRepository)
        {
            _componentRepository = componentRepository;
        }
        public override bool Visit(Folder folder)
        {
            return false;
        }

        public override bool Visit(File file)
        {
            var path = file.Metadata.GetPath();
            Data = _componentRepository.Read(new FileInfo(path));
            return false;
        }

    }
}
