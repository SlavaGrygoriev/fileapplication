﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Interfaces;
using File = FileApplication.Infrastructure.Domain.File;

namespace FileApplication.Infrastructure.Visitors
{
    public class LoadContainerVisitor : BaseComponentVisitor
    {
        private readonly IFileSystemComponentFactory _componentFactory;
        private readonly IFileSystemComponentRepository _componentRepository;

        public LoadContainerVisitor(IFileSystemComponentFactory componentFactory, IFileSystemComponentRepository componentRepository)
        {
            _componentFactory = componentFactory;
            _componentRepository = componentRepository;
        }
        private void ProcessContainer(IFileSystemContainer container)
        {
            var folderInfo = new FolderInfo() {Name = container.Metadata.GetPath()};

            var files = _componentRepository.GetFiles(folderInfo);
            foreach (var fileInfo in files)
            {
                var fileName = fileInfo.Name.ExtractName();
                var fileComponent = _componentFactory.CreateFile(fileName);
                container.Add(fileComponent);
            }

            var folders = _componentRepository.GetFolders(folderInfo);
            foreach (var folder in folders)
            {
                var folderName = folder.Name.ExtractName();
                var folderComponent = _componentFactory.CreateFolder(folderName);
                container.Add(folderComponent);
                ProcessContainer(folderComponent);
            }

        }

        public override bool Visit(Folder folder)
        {
           
            if (!folder.IsRoot) //load specific folder by path
            {
                
                var folderInfo = new FolderInfo()
                {
                    Name = folder.Metadata.GetPath()
                };
                if (!_componentRepository.Exists(folderInfo))
                {
                    throw new KeyNotFoundException(); //todo: create custom
                }

                folder.Metadata.SetPath(folderInfo.Name);
            }

            while (folder.ChildComponents.Any())
            {
                folder.Remove(folder.ChildComponents.First());
            }
            ProcessContainer(folder);
           
            return false;
        }

        public override bool Visit(File file)
        {
            return false;
        }
       
    }
}
