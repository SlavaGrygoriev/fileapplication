﻿using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Interfaces;

namespace FileApplication.Infrastructure.Visitors
{
    public class RenameComponentVisitor : BaseComponentVisitor
    {
        private readonly IFileSystemComponentRepository _componentRepository;
        private readonly string _newName;

        public RenameComponentVisitor(IFileSystemComponentRepository componentRepository,string newName)
        {
            _componentRepository = componentRepository;
            _newName = newName;
        }

        public override bool Visit(Folder folder)
        {
            var folderInfo = new FolderInfo() { Name = folder.Metadata.GetPath() };
            _componentRepository.Rename(folderInfo, _newName);
            folder.Name = _newName;
            return false;
        }

        public override bool Visit(File file)
        {
            var fileInfo = new FolderInfo() { Name = file.Metadata.GetPath() };
            _componentRepository.Rename(fileInfo, _newName);
            file.Name = _newName;
            return false;
        }

    }
}
