﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Interfaces;
using File = FileApplication.Infrastructure.Domain.File;
using FileInfo = FileApplication.Infrastructure.Domain.FileInfo;

namespace FileApplication.Infrastructure.Visitors
{
    public class FindComponentVisitor : BaseComponentVisitor 
    {
        private readonly IFileSystemComponentRepository _componentRepository;
        private readonly string _path;

        public FindComponentVisitor(IFileSystemComponentRepository componentRepository, string path)
        {
            _componentRepository = componentRepository;
            _path = path;
        }

   
        public override bool Visit(Folder folder)
        {
            var path = Path.Combine(_path, folder.Name); //todo: remove reference to IO
            var folderInfo = new FolderInfo()
            {
                Name = path
            };
            if (!_componentRepository.Exists(folderInfo))
            {
                throw new KeyNotFoundException(); //todo: create custom
            }
            folder.Metadata.SetPath(path);
            return false;
        }

        public override bool Visit(File file)
        {
            var path = Path.Combine(_path, file.Name); //todo: remove reference to IO
            var info = new FileInfo()
            {
                Name = path
            };
            if (!_componentRepository.Exists(info))
            {
                throw new KeyNotFoundException(); //todo: create custom
            }
            file.Metadata.SetPath(path);
            return false;
        }

    }
}
