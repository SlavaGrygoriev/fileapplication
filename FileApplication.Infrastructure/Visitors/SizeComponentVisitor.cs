﻿using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Interfaces;

namespace FileApplication.Infrastructure.Visitors
{
    public class SizeComponentVisitor : BaseComponentVisitor
    {
        private readonly IFileSystemComponentRepository _componentRepository;

        public SizeComponentVisitor(IFileSystemComponentRepository componentRepository)
        {
            _componentRepository = componentRepository;
        }

        public long Size { get; private set; }

        public override bool Visit(Folder folder)
        {
            return true;
        }

        public override bool Visit(File file)
        {
            
            long size = _componentRepository.GetSize(new FileInfo(file.Metadata.GetPath()));
            file.Metadata.SetSize(size);
            Size += size;
            return true;
        }
       
    }
}
