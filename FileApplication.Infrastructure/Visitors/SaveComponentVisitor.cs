﻿using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Interfaces;
using FileApplication.Infrastructure.Repositories;

namespace FileApplication.Infrastructure.Visitors
{
    public class SaveComponentVisitor : BaseComponentVisitor
    {
        private readonly IFileSystemComponentRepository _componentRepository;

        public SaveComponentVisitor(IFileSystemComponentRepository componentRepository)
        {
            _componentRepository = componentRepository;
        }

        public override bool Visit(Folder folder)
        {
            var path = folder.Metadata.GetPath();
            var folderInfo = new FolderInfo()
            {
                Name = path
            };
            if (!_componentRepository.Exists(folderInfo))
            {
                _componentRepository.Create(folderInfo);
            }

            return true;
        }

        public override bool Visit(File file)
        {
            var path = file.Metadata.GetPath();
            var info = new FileInfo()
            {
                Name = path,
            };
            if (!_componentRepository.Exists(info) || file.Data != null)
            {
                _componentRepository.Create(info,file.Data);
            }
            return true;
        }
       
    }
}
