﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileApplication.Infrastructure.Interfaces;
using FileApplication.Infrastructure.Repositories;
using FileApplication.Infrastructure.Visitors;

namespace FileApplication.Infrastructure.Factories
{
    public class FileSystemVisitorFactory : IFileSystemVisitorFactory
    {
        private readonly FileSystemComponentRepository _componentRepository;
        private readonly IFileSystemComponentFactory _componentFactory;

        public FileSystemVisitorFactory(FileSystemComponentRepository componentRepository,IFileSystemComponentFactory componentFactory)
        {
            _componentRepository = componentRepository;
            _componentFactory = componentFactory;
        }

        public IFileSystemVisitor LoadContainer()
        {
            var visitor = new LoadContainerVisitor(_componentFactory,_componentRepository);
            return visitor;
        }

        public IFileSystemVisitor Save()
        {
            var visitor = new SaveComponentVisitor(_componentRepository);
            return visitor;
        }

        public GetChildComponentVisitor<T> GetChild<T>(Func<T, bool> expr) where T : class, IFileSystemComponent
        {
            var visitor = new GetChildComponentVisitor<T>(expr);
            return visitor;
        }

        public IFileSystemVisitor Delete()
        {
            var visitor = new DeleteComponentVisitor(_componentRepository);
            return visitor;
        }

        public IFileSystemVisitor Rename(string newName)
        {
            var visitor = new RenameComponentVisitor(_componentRepository,newName);
            return visitor;
        }

        public CopyComponentVisitor Copy(Func<IFileSystemComponent, string> copyNameExpr = null)
        {
            var visitor = new CopyComponentVisitor(_componentFactory,_componentRepository,
                copyNameExpr ?? (r=> $"Copy Of {r.Name}"));
            return visitor;
        }
        public IFileSystemVisitor Write(byte[] data)
        {
            var visitor = new WriteDataVisitor(_componentRepository,data);
            return visitor;
        }

        public ReadDataVisitor Read()
        {
            var visitor = new ReadDataVisitor(_componentRepository);
            return visitor;
        }

        public SizeComponentVisitor Size()
        {
            return new SizeComponentVisitor(_componentRepository);
        }
    }
}
