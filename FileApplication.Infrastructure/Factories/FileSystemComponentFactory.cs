﻿using System;
using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Interfaces;

namespace FileApplication.Infrastructure.Factories
{
    public class FileSystemComponentFactory : IFileSystemComponentFactory
    {

        private void SetName(FileSystemComponent component,string name)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            var path = name;
            if (name.IsPath())
            {
                name = name.ExtractName();
            }

            component.Name = name;
            component.Metadata.SetPath(path);

        }

        public IFileSystemContainer CreateFolder(string name)
        {
            var component = new Folder();
            SetName(component,name);
            return component;
        }

        public IFileSystemComponent CreateFile(string name)
        {
            var component = new File();
            SetName(component, name);
            return component;
        }

        public IFileSystemContainer CreateRoot()
        {
            var folder = new Folder(string.Empty)
            {
                IsRoot = true
            };
            return folder;
        }

    }
}
