﻿using System;
using FileApplication.Infrastructure.Interfaces;

namespace FileApplication.Infrastructure.Providers
{
    public class LocalStoragePathProvider : ILocalStoragePathProvider
    {
        public LocalStoragePathProvider(string path)
        {
            Path = path ?? throw new ArgumentNullException(nameof(path));
        }

        public string Path { get; }

    }
}
