﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using FileApplication.Infrastructure.Domain;
using FileApplication.Infrastructure.Interfaces;
using Directory = System.IO.Directory;
using File = System.IO.File;
using FileInfo = FileApplication.Infrastructure.Domain.FileInfo;

namespace FileApplication.Infrastructure.Repositories
{
    public class FileSystemComponentRepository : IFileSystemComponentRepository
    {
        private readonly ILocalStoragePathProvider _localStoragePathProvider;
        private DirectoryInfo _structureDirectory;

        public FileSystemComponentRepository(ILocalStoragePathProvider localStoragePathProvider)
        {
            _localStoragePathProvider = localStoragePathProvider;
            InitFolders();


        }

        private DirectoryInfo CreateDir(string path)
        {
            return Directory.Exists(path) ? new DirectoryInfo(path) : Directory.CreateDirectory(path);
        }

        private void InitFolders()
        {
            var rootPath = _localStoragePathProvider.Path;
            var structurePath = Path.Combine(rootPath, "tree");

            CreateDir(rootPath);
            _structureDirectory = CreateDir(structurePath);
        }

        private string GetAbsolutePath(string relativePath)
        {
            return Path.Combine(_structureDirectory.FullName, relativePath.Trim('\\'));
        }
     
        public IEnumerable<FolderInfo> GetFolders(FolderInfo container)
        {
            var parent = new DirectoryInfo(GetAbsolutePath(container.Name));
            var dirs = parent.GetDirectories();
            return dirs.Select(r => new FolderInfo()
            {
                Name = Path.Combine(container.Name, r.Name)
            });
        }

        public IEnumerable<FileInfo> GetFiles(FolderInfo container)
        {
            var parent = new DirectoryInfo(GetAbsolutePath(container.Name));
            var dirs = parent.GetFiles();
            return dirs.Select(r => new FileInfo()
            {
                Name = Path.Combine(container.Name, r.Name)
            });
        }


        public void Create(FolderInfo container)
        {
            Directory.CreateDirectory(GetAbsolutePath(container.Name));

        }

        public bool Exists(FolderInfo container)
        {
            return Directory.Exists(GetAbsolutePath(container.Name));
        }

        public bool Exists(FileInfo file)
        {
            return File.Exists(GetAbsolutePath(file.Name));
        }

        public void Create(FileInfo fileInfo,byte[] data)
        {
            var path = GetAbsolutePath(fileInfo.Name);
            if (data == null)
            {
                using (var fs = File.Create(path))
                {
                }

            }
            else
            {
                using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    fs.Write(data, 0, data.Length);
                }
            }
        }

        public byte[] Read(FileInfo fileInfo)
        {
            var path = GetAbsolutePath(fileInfo.Name);
            var data = File.ReadAllBytes(path);
            return data;
        }

        public void Delete(FileInfo fileInfo)
        {
            var path = GetAbsolutePath(fileInfo.Name);
            File.Delete(path);
        }
        public void Delete(FolderInfo info)
        {
            var path = GetAbsolutePath(info.Name);
            Directory.Delete(path,true);
        }

        private string GetPathToMove(string path, string newName)
        {
            var parts = path.Split(Path.DirectorySeparatorChar);
            parts[parts.Length - 1] = newName;
            var newPath = string.Join(Path.DirectorySeparatorChar.ToString(), parts);
            return newPath;
        }

        public void Rename(FileInfo info,string newName)
        {
            var path = GetAbsolutePath(info.Name);
            var newPath = GetPathToMove(path,newName);
            File.Move(path, newPath);
        }
        public void Rename(FolderInfo info, string newName)
        {
            var path = GetAbsolutePath(info.Name);
            var newPath = GetPathToMove(path, newName);
            Directory.Move(path, newPath);
        }

        public long GetSize(FileInfo fileInfo)
        {
            var path = GetAbsolutePath(fileInfo.Name);
            return new System.IO.FileInfo(path).Length;
        }
    }

}
